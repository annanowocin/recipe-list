import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RecipeListComponent} from './recipe-list/recipe-list.component';

const routes: Routes = [
  {path: 'recipe/:id', component: RecipeListComponent},
  {path: '**', component: RecipeListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
