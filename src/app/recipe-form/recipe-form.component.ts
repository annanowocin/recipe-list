import {Component, EventEmitter, Input, Output} from '@angular/core';
import {RecipeFormService} from './services/recipe-form.service';
import {Recipe} from '../common/interfaces/recipe.interface';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.scss']
})
export class RecipeFormComponent {

  @Input() recipes: Recipe[] = [];
  @Output() submitted = new EventEmitter();

  id: string;
  name: string;
  bigAmount: boolean;

  constructor(private recipeFormService: RecipeFormService) {
  }

  isIdUnique() {
    let result = true;
    this.recipes.map((item) => {
      if (item.id === this.id) {
        result = false;
      }
      return item;
    });
    return result;
  }

  submit(form: NgForm) {
    if (!form.form.valid || !this.isIdUnique()) {
      return;
    }
    this.recipeFormService.addRecipe({
      id: this.id,
      name: this.name,
      bigAmount: this.bigAmount,
      ingredients: []
    }).subscribe(() => {
      form.resetForm();
      this.submitted.emit();
    });
  }

}
