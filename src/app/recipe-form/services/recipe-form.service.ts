import {Injectable} from '@angular/core';
import {HttpService} from '../../common/services/http.service';
import {Recipe} from '../../common/interfaces/recipe.interface';

@Injectable({
  providedIn: 'root'
})
export class RecipeFormService {

  constructor(private httpService: HttpService) {
  }

  addRecipe(data: Recipe) {
    return this.httpService.post('recipes', data);
  }
}
