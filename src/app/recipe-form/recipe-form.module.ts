import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipeFormComponent} from './recipe-form.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [RecipeFormComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [RecipeFormComponent]
})
export class RecipeFormModule {
}
