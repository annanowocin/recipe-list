export interface IngredientName {
  id: string;
  name: string;
}
