import {Ingredient} from './ingredient.interface';

export interface Recipe {
  id: string;
  name: string;
  bigAmount: boolean;
  ingredients: Ingredient[];
}
