import { TestBed } from '@angular/core/testing';

import { HttpMockService } from './http-mock.service';

describe('HttpMockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpMockService = TestBed.get(HttpMockService);
    expect(service).toBeTruthy();
  });
});
