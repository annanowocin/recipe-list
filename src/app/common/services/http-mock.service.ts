import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Recipe} from '../interfaces/recipe.interface';

@Injectable({
  providedIn: 'root'
})
export class HttpMockService {

  recipes = [];

  constructor() {
    const defaultRecipes: Recipe[] = [
      {id: '1', name: 'Sernik', bigAmount: false, ingredients: []},
      {id: '2', name: 'Jabłecznik', bigAmount: false, ingredients: []},
    ];
    try {
      this.recipes = JSON.parse(localStorage.getItem('recipes')) || defaultRecipes;
    } catch (e) {
      this.recipes = defaultRecipes;
      this.saveData();
    }
  }

  get(url: string) {
    let response = Observable.create(observer => {
      observer.error({status: 404});
      observer.complete();
    });

    if (url === 'recipes') {
      response = Observable.create(observer => {
        observer.next(this.recipes);
        observer.complete();
      });
    }

    if (url === 'ingredients') {
      response = Observable.create(observer => {
        observer.next([
          {id: '1', name: 'flour'},
          {id: '2', name: 'oil'},
          {id: '3', name: 'egs'},
          {id: '4', name: 'cheese'},
          {id: '5', name: 'chicken'},
          {id: '6', name: 'potatoes'},
          {id: '7', name: 'tomatoes'},
          {id: '8', name: 'sugar'},
          {id: '9', name: 'bread'},
          {id: '10', name: 'butter'},
          {id: '11', name: 'ham'},
          {id: '12', name: 'onion'},
          {id: '13', name: 'carrots'},
          {id: '14', name: 'rice'},
          {id: '15', name: 'soy souce'},
          {id: '16', name: 'yeast'},
          {id: '17', name: 'cream'},
          {id: '18', name: 'water'},
          {id: '19', name: 'milk'},
          {id: '20', name: 'lettuce'},
        ]);
        observer.complete();
      });
    }

    if (url === 'cookingMethods') {
      response = Observable.create(observer => {
        observer.next([
          {id: '1', name: 'bake'},
          {id: '2', name: 'fry'},
          {id: '3', name: 'roast'},
          {id: '4', name: 'grill'},
          {id: '5', name: 'steam'},
          {id: '6', name: 'poach'},
          {id: '7', name: 'simmer'},
          {id: '8', name: 'broil'},
          {id: '9', name: 'blanch'},
          {id: '10', name: 'braised'},
          {id: '11', name: 'stew'},
        ]);
        observer.complete();
      });
    }

    if (url.indexOf('recipe/') !== -1) {
      response = Observable.create(observer => {
        const idToGet =  url.replace(/.*recipe\//, '');
        const result = this.recipes.filter((item) => {
          return item.id === idToGet;
        });
        if (result[0]) {
          observer.next(result[0]);
        } else {
          observer.error({status: 404});
        }
        observer.complete();
      });
    }

    return response;
  }

  post(url: string, data: any) {
    return Observable.create(observer => {
      if (url === 'recipes') {
        this.recipes.push(data);
        this.saveData();
      }
      observer.next();
      observer.complete();
    });
  }

  put(url: string, data: any) {
    return Observable.create(observer => {
      if ((/recipe\/[^\/]+\/ingredients/).test(url)) {
        const recipeId = url.match(/recipe\/([^\/]+)\/ingredients/)[1];
        this.recipes = this.recipes.map((item: Recipe) => {
          if (item.id === recipeId) {
            item.ingredients.push(data);
          }
          return item;
        });
        this.saveData();
      }
      observer.next();
      observer.complete();
    });
  }

  delete(url: string) {
    return Observable.create(observer => {
      if (url.indexOf('recipe/') !== -1) {
        const idToRemove =  url.replace(/.*recipe\//, '');
        this.recipes = this.recipes.filter((item) => {
          return item.id !== idToRemove;
        });
        this.saveData();
      }
      observer.next();
      observer.complete();
    });
  }

  private saveData() {
    localStorage.setItem('recipes', JSON.stringify(this.recipes));
  }
}
