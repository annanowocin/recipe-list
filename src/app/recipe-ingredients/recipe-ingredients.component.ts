import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Recipe} from '../common/interfaces/recipe.interface';
import {CookingMethod} from '../common/interfaces/cooking-method.interface';
import {RecipeIngredientsService} from './services/recipe-ingredients.service';
import {IngredientName} from '../common/interfaces/ingredient-name.interface';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-recipe-ingredients',
  templateUrl: './recipe-ingredients.component.html',
  styleUrls: ['./recipe-ingredients.component.scss']
})
export class RecipeIngredientsComponent implements OnInit {

  @Input()
  set recipe(val: Recipe) {
    this.recipeData = val;
  }

  @Output()
  submitted = new EventEmitter();

  id: string;
  name: string;
  amount: number;
  unit: string;
  cookingMethod: string;
  recipeData: Recipe = null;
  cookingMethods: CookingMethod[] = [];
  cookingMethodsById = [];
  ingredients: IngredientName[] = [];
  ingredientsById = [];

  constructor(private recipeIngredientsService: RecipeIngredientsService) {
  }

  ngOnInit() {
    this.recipeIngredientsService.getCookingMethods().subscribe((data: CookingMethod[]) => {
      this.cookingMethods = data;
      this.cookingMethods.map((item) => {
        this.cookingMethodsById[item.id] = item.name;
      });
    });
    this.recipeIngredientsService.getIngredients().subscribe((data: IngredientName[]) => {
      this.ingredients = data;
      this.ingredients.map((item) => {
        this.ingredientsById[item.id] = item.name;
      });
    });
  }

  isIdUnique() {
    let result = true;
    this.recipeData.ingredients.map((item) => {
      if (item.id === this.id) {
        result = false;
      }
      return item;
    });
    return result;
  }

  isToMuchIngredients() {
    let ingredientsSum = 0;
    this.recipeData.ingredients.map((item) => {
      ingredientsSum += item.amount || 0;
      return item;
    });
    return (ingredientsSum + (this.amount || 0) > 1000) && !this.recipeData.bigAmount;
  }

  submit(form: NgForm) {
    if (!form.form.valid || !this.isIdUnique()) {
      return;
    }
    this.recipeIngredientsService.addIngredient(this.recipeData.id, {
      id: this.id,
      name: this.name,
      amount: this.amount,
      unit: this.unit,
      cookingMethod: this.cookingMethod
    }).subscribe(() => {
      form.resetForm();
      this.submitted.emit();
    });
  }
}
