import {Injectable} from '@angular/core';
import {HttpService} from '../../common/services/http.service';
import {Ingredient} from '../../common/interfaces/ingredient.interface';

@Injectable({
  providedIn: 'root'
})
export class RecipeIngredientsService {

  constructor(private httpService: HttpService) {
  }

  getCookingMethods() {
    return this.httpService.get('cookingMethods');
  }

  getIngredients() {
    return this.httpService.get('ingredients');
  }

  addIngredient(recipeId: string, ingredient: Ingredient) {
    return this.httpService.put(`recipe/${recipeId}/ingredients`, ingredient);
  }
}
