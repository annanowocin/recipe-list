import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipeIngredientsComponent} from './recipe-ingredients.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [RecipeIngredientsComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [RecipeIngredientsComponent]
})
export class RecipeIngredientsModule {
}
