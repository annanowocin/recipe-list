import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipeDetailsComponent} from './recipe-details.component';
import {RecipeIngredientsModule} from '../recipe-ingredients/recipe-ingredients.module';

@NgModule({
  declarations: [RecipeDetailsComponent],
  imports: [
    CommonModule,
    RecipeIngredientsModule
  ],
  exports: [RecipeDetailsComponent]
})
export class RecipeDetailsModule {
}
