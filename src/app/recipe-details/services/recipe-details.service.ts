import {Injectable} from '@angular/core';
import {HttpService} from '../../common/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeDetailsService {

  constructor(private httpService: HttpService) {
  }

  getRecipe(id) {
    return this.httpService.get(`recipe/${id}`);
  }
}
