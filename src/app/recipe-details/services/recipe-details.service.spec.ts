import { TestBed } from '@angular/core/testing';

import { RecipeDetailsService } from './recipe-details.service';

describe('RecipeDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecipeDetailsService = TestBed.get(RecipeDetailsService);
    expect(service).toBeTruthy();
  });
});
