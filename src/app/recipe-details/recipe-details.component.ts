import {Component, Input} from '@angular/core';
import {RecipeDetailsService} from './services/recipe-details.service';
import {Recipe} from '../common/interfaces/recipe.interface';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.scss']
})
export class RecipeDetailsComponent {

  @Input()
  set id(val: string) {
    this.idValue = val;
    this.getRecipe(val);
  }

  idValue = null;
  recipe: Recipe = null;
  errorCode = 0;

  constructor(private recipeDetailsService: RecipeDetailsService) { }

  getRecipe(id: string) {
    this.errorCode = 0;
    this.recipeDetailsService.getRecipe(id).subscribe((data: Recipe) => {
      this.recipe = data;
    }, (error) => {
      this.errorCode = error.status;
    });
  }

  refresh() {
    this.getRecipe(this.idValue);
  }
}
