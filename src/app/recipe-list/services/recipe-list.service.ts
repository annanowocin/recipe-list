import {Injectable} from '@angular/core';
import {HttpService} from '../../common/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeListService {

  constructor(private httpService: HttpService) {
  }

  getRecipes() {
    return this.httpService.get('recipes');
  }

  removeRecipe(id) {
    return this.httpService.delete(`recipe/${id}`);
  }
}
