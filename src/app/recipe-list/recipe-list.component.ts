import {Component, OnInit} from '@angular/core';
import {Recipe} from '../common/interfaces/recipe.interface';
import {RecipeListService} from './services/recipe-list.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [];
  selectedRecipe: string = null;

  constructor(private recipesListService: RecipeListService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.route.snapshot.params.id) {
      this.selectedRecipe = this.route.snapshot.params.id;
    }
    this.refreshList();
  }

  refreshList() {
    this.recipesListService.getRecipes().subscribe((data: Recipe[]) => {
      this.recipes = data;
    });
  }

  openRecipe(id: string) {
    this.selectedRecipe = id;
    this.router.navigate(['recipe', id]);
  }

  removeRecipe(id: string) {
    this.recipesListService.removeRecipe(id).subscribe(() => {
      this.refreshList();
      this.router.navigate(['']);
    });
  }

}
