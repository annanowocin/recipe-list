import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipeListComponent} from './recipe-list.component';
import {RecipeDetailsModule} from '../recipe-details/recipe-details.module';
import {RecipeFormModule} from '../recipe-form/recipe-form.module';

@NgModule({
  declarations: [RecipeListComponent],
  imports: [
    CommonModule,
    RecipeDetailsModule,
    RecipeFormModule
  ]
})
export class RecipeListModule {
}
